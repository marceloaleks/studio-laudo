var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

var paths = {
    'bootstrap': './vendor/bower_components/bootstrap-sass-official/assets/'
}

elixir(function (mix) {

    mix.sass('app.scss');

    mix.copy('vendor/bower_components/bootstrap/dist', 'public/vendor/bootstrap');
    mix.copy('vendor/bower_components/jquery/dist', 'public/vendor/jquery');
    mix.copy('vendor/bower_components/normalize.css/normalize.css', 'public/vendor/normalize');
    mix.copy('vendor/bower_components/toastr/toastr.css', 'public/vendor/toastr');
    mix.copy('vendor/bower_components/toastr/toastr.js', 'public/vendor/toastr');

    mix.copy('resources/assets/js/App.js', 'public/js');

    //datatables.net
    mix.copy('vendor/bower_components/datatables/media/js/*.js', 'public/vendor/datatables/js/');
    mix.copy('vendor/bower_components/datatables/media/css/jquery.dataTables.min.css', 'public/vendor/datatables/css');
    mix.copy('vendor/bower_components/datatables/media/images', 'public/vendor/datatables/images');
    mix.copy('vendor/bower_components/datatables-plugins/integration', 'public/vendor/datatables/plugins');

    //semantic-ui
    mix.copy('vendor/bower_components/semantic-ui/dist', 'public/vendor/semantic-ui');

    /** @TODO */

    //ckeditor
    mix.copy('vendor/bower_components/ckeditor/lang', 'public/vendor/ckeditor/lang');
    mix.copy('vendor/bower_components/ckeditor/plugins', 'public/vendor/ckeditor/plugins');
    mix.copy('vendor/bower_components/ckeditor/skins', 'public/vendor/ckeditor/skins');
    mix.copy('vendor/bower_components/ckeditor/ckeditor.js', 'public/vendor/ckeditor/');
    mix.copy('vendor/bower_components/ckeditor/styles.js', 'public/vendor/ckeditor/');
    mix.copy('vendor/bower_components/ckeditor/*.css', 'public/vendor/ckeditor/');
    //O arquivo 
    //Outros plugins que estão no diretório resources
    mix.copy('vendor/bower_components/nckeditor/config.js', 'public/js/ckeditorconf.js');
    mix.copy('resources/assets/ckeditor/', 'public/vendor/ckeditor/');

//        mix.version('public/vendor/ckeditor/ckeditor.js');

    //##########################################################################
    //##      ATENÇÂO !!!!
    //#######
    //O versionamento de arquivos não aceita múltiplas chamadas de mix.version
    mix.version(['js/ckeditorconf.js', 'css/app.css']);
    //##########################################################################





});  