<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetavaloresTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        //
        Schema::create('metavalores', function(Blueprint $table) {
            $table->increments('id');  //chave primária


            $table->string('nome');      //Nome: Valor que aparecerá 
            $table->text('conteudo');    //Conteúdo que será inserido 
//            $table->softDeletes();       //Poderá ser removido e restaurado (será apenas marcado para remoção)
            $table->timestamps();  //Timestamps (são inseridos automaticamente) 
            
            
            //### Chaves estrangeiras ###
            $table->integer('metadado_id')->unsigned()->index();
            $table->foreign('metadado_id')->references('id')->on('metadados')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //### Remove a tabela 
        Schema::drop('metavalores');
    }

}
