<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetatiposTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('metatipos', function(Blueprint $table) {
            
            $table->increments('id');  //chave primária

            

            $table->timestamps();  //Timestamps (são inseridos automaticamente) 
        });
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
        Schema::drop('metatipos');
    }

}
