<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerfilTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('perfil', function(Blueprint $table) {
            $table->increments('id');  //chave primária

            $table->string('nome');
            $table->string('nomemeio');
            $table->string('sobrenome');
            
            
            $table->string('cpf');
            $table->string('sexo', 1);
            
            
            
            //### Chaves estrangeiras ###
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

                        
            $table->timestamps();  //Timestamps (são inseridos automaticamente) 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //### Remove a tabela 
        Schema::drop('perfil');
         
    }

}
