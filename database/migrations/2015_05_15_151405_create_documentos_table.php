<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('documentos', function(Blueprint $table) {
            $table->increments('id');  //chave primária

            $table->string('nome');      //Nome: Valor que aparecerá 
            $table->text('conteudo');    //Conteúdo que será inserido 
            
            
            
            //### Timestamps ###
            $table->timestamps();  //Timestamps (são inseridos automaticamente) 
            $table->softDeletes();  //Removido por soft (proteção)

            //### Chaves estrangeiras ###
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //Remove a tabela
        Schema::drop('documentos');
        
    }

}
