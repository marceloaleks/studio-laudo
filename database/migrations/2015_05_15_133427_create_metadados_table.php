<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetadadosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        //
        Schema::create('metadados', function(Blueprint $table) {
            $table->increments('id');  //chave primária

            $table->string('nome', 50);  //Nome do campo/dado
            $table->text('descricao');   //Descrição do metadado
            $table->text('valor');   //Valor ou valores (dentro de um json)
//            $table->softDeletes();       //Poderá ser removido e restaurado (será apenas marcado para remoção)
            $table->timestamps();  //Timestamps (são inseridos automaticamente) 

            
            //### Chaves estrangeiras ###
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //### Remove a tabela 
        Schema::drop('metadados');
    }

}
