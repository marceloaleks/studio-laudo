<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


/**
 * Cria a tabela que conterá a visibilidade discriminada para o objeto
 */
class CreateVisibilidadeTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        //
        Schema::create('visibilidade', function(Blueprint $table) {
            $table->increments('id');  //chave primária

            //Define a visibilidade
            $table->enum('visivelpor', ['id', 'grupo'] );
            
            //### Chaves estrangeiras ###
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();  //Timestamps (são inseridos automaticamente) 
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('visibilidade');
    }

}
