<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('modelos', function(Blueprint $table) {

            $table->increments('id');  //chave primária
            
            
            $table->string('nome');      //Nome: Valor que aparecerá 
            $table->text('conteudo');    //Conteúdo que será inserido 
            
            
            //Opões
            

            //### Chaves estrangeiras - Aponta para o modelo original ###
            $table->integer('origem_id')->unsigned()->index();
            $table->foreign('origem_id')->references('id')->on('modelos')->onDelete('cascade');
            
            $table->timestamps();  //Timestamps (são inseridos automaticamente) 
            
            //### Chaves estrangeiras ###
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        
        Schema::drop('modelos');
        
    }

}
