
    /**
     * Modifica a visibilidade de um objeto HTML
     * @param {type} objeto
     * @param {boolean} estado
     * @returns sem retorno
     */
    function setVisible(objeto, estado) {
        $(objeto).css({
            'display': estado ? 'block' : 'none'
        });
    }
    
    
    