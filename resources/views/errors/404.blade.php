<html>
    <head>
        <link href='http://fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

        <style>
            body {
                margin: 0;
                padding: 0;
                width: 100%;
                height: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                font-weight: 900;
                margin-bottom: 40px;
                color: red;


            }
        </style> 
    </head> 
    <body> 
        
        @lang('message.next')
        
        @choice('message.next', 2)
        
        
        
        {{ trans('message.next') }}

        
        {{--
        @if ( isset($errors) )
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{!! $error !!}</li>
                @endforeach
            </ul>
        </div>
        @endif
        --}}
        
        
        <div class="container">
            <div class="content">
                <div class="title">Página não encontrada</div>
            </div>
        </div>
    </body>
</html>
