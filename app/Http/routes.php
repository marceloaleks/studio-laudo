<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', 'WelcomeController@index');
Route::get ('/', function() {
   return 'alo';
    
});


Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);




Route::get ("/erro", function() {
    return view("/errors.404")->withErrors(['teste1', '2' => 'teste2']);
});


Route::get ("/", function() {
    return view("/privado/meta/campos");
});
